import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="analyzer",
    author="Andrij David",
    author_email="andrijdavid@gmail.com",
    long_description=long_description,
    long_description_content_type="text/markdown",
    description="Detect and Extract irregularity of roads from digital elevation model",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    version_config=True,
    setup_requires=['setuptools-git-versioning'],
    install_requires=[

    ]
)
