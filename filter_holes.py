import geopandas as gpd
import rasterio as rs
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--input', help="Path to the shapefile.")
parser.add_argument('--output', help="Output shapefile.", default="output/holes_filtered.shp")
parser.add_argument('--perimeter', type=float, help="Minimum perimeter in meter. Default is 10 cm.", default=0.1)
parser.add_argument('--surface', type=float, help="Minimum surface in square meter. Default is 10 cm2.", default=0.001)
parser.add_argument('--volume', type=float, help="Minimum volume in meter3. Default is 8 cm3.", default=0.000008)
parser.add_argument('--depth', type=float, help="Minimum depth in meter. Default is 6 cm.", default=0.06)

if __name__ == '__main__':

    args = parser.parse_args()

    gdf = gpd.GeoDataFrame.from_file(args.input)

    # May be snapping

    print("Before")
    print("P:", gdf["perimeter"].min())
    print("S:", gdf["surface"].min())
    print("D:", gdf["depth"].min())
    print("V:", gdf["volume"].min())

    gdf = gdf[gdf["perimeter"] > args.perimeter]
    gdf = gdf[gdf["surface"] > args.surface]
    gdf = gdf[gdf["volume"] > args.volume]
    gdf = gdf[gdf["depth"] > args.depth]

    print("After")
    print("P:", gdf["perimeter"].min(), " ", gdf["perimeter"].median()," ", gdf["perimeter"].max())
    print("S:", gdf["surface"].min(), " ", gdf["surface"].median()," ", gdf["surface"].max())
    print("D:", gdf["depth"].min(), " ", gdf["depth"].median()," ", gdf["depth"].max())
    print("V:", gdf["volume"].min()," ", gdf["volume"].median()," ", gdf["volume"].max())

    gdf.to_file(args.output)
