from dem.parser import *
from multiprocessing import Pool
import multiprocessing
from supplementary_scripts.generateDEM import generate_DEM, execute_pdal_pipeline
from pathlib import Path
from osgeo import gdal
import argparse
import time
import glob
import os
import pandas as pd
import geopandas as gpd
import gc
import uuid

gdal.SetCacheMax(134217728)

parser = argparse.ArgumentParser()
parser.add_argument('--input', help="Path to the input should be point cloud.",
                    default="/home/andrijdavid/Downloads/spolu_100per_red_ocisttene.ply")
parser.add_argument('--process', help="Number of process", type=int,
                    default=multiprocessing.cpu_count())
parser.add_argument('--dem-output', help="Dem output file path.",
                    default="output/scan_cleaned_#.tif")
parser.add_argument('--output', help="Output file path for the shapefile.", default="output/holes_#.shp")


def process_dem(dem):
    gc.collect()
    print(f"Processing {dem}")
    outpath = Path(args.output.replace("#", str(dem.split("_cleaned_")[-1].split(".tif")[0])))
    outpath.parent.mkdir(exist_ok=True, parents=True)
    ouster_dem = rs.open(dem, nodata=-9999)
    surface = ouster_dem.shape[0] * ouster_dem.shape[1]
    if surface < 10:
        # Too small surface
        print("Too small surface")
        return None

    road_convex_hull = segment_road(ouster_dem, min_length=30).convex_hull
    if len(road_convex_hull.bounds) < 4:
        # No road found
        print("No road found")
        return None
    pgons = extract_irregularities(ouster_dem, road_convex_hull)
    gdf = save_poly_to_file(pgons, fp=outpath.name, surface=True, perimiter=True, path=outpath.parent)
    gdf = gdf[gdf["perimeter"] > 0]
    gdf = gdf[gdf["surface"] > 0]
    gdf = add_metadata(gdf, ouster_dem, out=outpath)
    # compute_uci(gdf, road_convex_hull)
    return gdf

#
# def process_dem(dem):
#     try:
#         gc.collect()
#         print(f"Processing {dem}")
#         outpath = Path(args.output.replace("#", str(i + 1)))
#         outpath.parent.mkdir(exist_ok=True, parents=True)
#         ouster_dem = rs.open(dem, nodata=-9999)
#         road_convex_hull = segment_road(ouster_dem, min_length=30).convex_hull
#         pgons = extract_irregularities(ouster_dem, road_convex_hull)
#         gdf = save_poly_to_file(pgons, fp=outpath.name, surface=True, perimiter=True, path=outpath.parent)
#         gdf = gdf[gdf["perimeter"] > 0]
#         gdf = gdf[gdf["surface"] > 0]
#         gdf = add_metadata(gdf, ouster_dem, out=outpath)
#         # compute_uci(gdf, road_convex_hull)
#         return gdf
#     except:
#         print(f"Error processing {dem} probably too small section")
#         return None


if __name__ == '__main__':
    args = parser.parse_args()
    print(args)
    start = time.time()
    pipeline_list = generate_DEM(args.input, args.dem_output)
    execute_pdal_pipeline(pipeline_list)
    gc.collect()
    dems = glob.glob(args.dem_output.replace("#", "*"))
    # dem_merge = args.dem_output.replace("_#", "")
    # dem_merge_vrt = args.dem_output.replace("_#.tif", ".vrt")
    #
    # print("Merging GeoTiff")
    #
    # cmd = f'gdalbuildvrt {dem_merge_vrt} {" ".join(dems)}'
    # print("Calling: ", cmd)
    # os.system(cmd)
    # cmd = f'gdal_translate {dem_merge_vrt} {dem_merge}'
    # print("Calling: ", cmd)
    # os.system(cmd)
    gdfs = []
    cpu = args.process
    multiprocessing.freeze_support()
    with Pool(cpu) as p:
        gdfs.extend(p.imap_unordered(process_dem, dems, max(1, len(dems) // cpu)))

    # Merge all the gdf
    gdfs = [g for g in gdfs if isinstance(g, pd.DataFrame)]
    gdf = gpd.GeoDataFrame(pd.concat(gdfs, ignore_index=True), crs=gdfs[0].crs)
    outpath = Path(args.output.replace("_#", ""))
    gdf.to_file(str(outpath))
    end = time.time()
    print(end - start, " time for execution")
