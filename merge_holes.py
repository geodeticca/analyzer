import geopandas as gpd
import rasterio as rs
import argparse
from pathlib import Path
import pandas as pd

def filter_files(files, include=[], exclude=[]):
    for incl in include:
        files = [f for f in files if incl in f.name]
    for excl in exclude:
        files = [f for f in files if excl not in f.name]
    return sorted(files)

def ls(x, recursive=False, include=[], exclude=[]):
    if not recursive:
        out = list(x.iterdir())
    else:
        out = [o for o in x.glob('**/*')]
    out = filter_files(out, include=include, exclude=exclude)
    return out

Path.ls = ls
parser = argparse.ArgumentParser()
parser.add_argument('--input', help="Path to the directory countaining the shapefiles.")
parser.add_argument('--output', help="Output shapefile.", default="output/holes.shp")

if __name__ == '__main__':
    args = parser.parse_args()
    inputpath = Path(args.input)
    shapes = inputpath.ls(recursive=True, include=['.shp'])
    gdfs = [gpd.GeoDataFrame.from_file(s) for s in shapes]
    gdf = gpd.GeoDataFrame(pd.concat(gdfs, ignore_index=True), crs=gdfs[0].crs)
    outpath = Path(args.output)
    gdf.to_file(str(outpath))
