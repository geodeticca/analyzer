## Installation
`pip install dem-analyzer --extra-index-url https://__token__:UePCwsHCeY-MCAeudWbM@gitlab.com/api/v4/projects/22272294/packages/pypi/simple`
`pip install ouster-reconstruction --extra-index-url https://__token__:UePCwsHCeY-MCAeudWbM@gitlab.com/api/v4/projects/22272430/packages/pypi/simple`
* GDAL https://mothergeo-py.readthedocs.io/en/latest/development/how-to/gdal-ubuntu-pkg.html
* pdal 

## How to use 



Arguments
``` 
  --input        Path to the input should be point cloud.
  --dem-output   Output file path for the intermediary Digital Elevation Model.
  --output       Output file path for the shapefile.
```